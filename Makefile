PROJECT = mpm

.PHONY: all mpm copy_file clean mercury-json

all: copy_file

mpm: mercury-json
	cd ./src && \
	mmc --search-lib-files-dir ../.mpm/packages/mercury-json/src \
	    --init-file ../.mpm/packages/mercury-json/src/mercury_json.init \
	    --link-object ../.mpm/packages/mercury-json/src/libmercury_json.a \
	    --make ${PROJECT}


mercury-json: ./.mpm/packages/mercury-json/src/libmercury_json.a

./.mpm/packages/mercury-json/src/libmercury_json.a: ./.mpm/packages/mercury-json
	cd ./.mpm/packages/mercury-json && ${MAKE}

./.mpm/packages/mercury-json: ./.mpm/packages
	cd ./.mpm/packages && git clone https://github.com/juliensf/mercury-json.git
	cd ./.mpm/packages/mercury-json && git checkout mercury_14_01

./.mpm/packages:
	mkdir -p .mpm/packages


copy_file: mpm
	@cp ./src/mpm ./

clean:
	rm -rf ./src/Mercury
	rm -f ./src/*.err
	rm -f ./src/*.mh
	rm -f ${PROJECT}
	rm -f ./src/${PROJECT}
