:- module mpm.commands.
:- interface.
:- import_module io.
:- import_module maybe.

:- type command
        ---> build
        ;    help
        ;    init
        ;    clean
        ;    real_clean
        ;    version.




:- pred from_command_line(maybe.maybe(command)::out, io::di, io::uo) is det.









:- implementation.
:- import_module io.
:- import_module list.

:- pred from_arg_list(list(string)::in, command::out) is semidet.
from_arg_list(ArgList, Command) :-
  (
    ["--help" : _] = ArgList,
    Command = help
  ;
    ["help" : _] = ArgList,
    Command = help
  ;
    ["--version" : _] = ArgList,
    Command = version
  ;
    ["version" : _] = ArgList,
    Command = version
  ;
    ["build" : _] = ArgList,
    Command = build
  ;
    ["make" : _] = ArgList,
    Command = build
  ;
    ["clean" : _] = ArgList,
    Command = clean
  ;
    ["realclean" : _] = ArgList,
    Command = real_clean
  ;
    ["init" : _] = ArgList,
    Command = init
  ).


from_command_line(MaybeCommand, !IO) :-
  io.command_line_arguments(ArgList, !IO),

  (
    if from_arg_list(ArgList, Command)
    then MaybeCommand = maybe.yes(Command)
    else MaybeCommand = maybe.no
  ).
