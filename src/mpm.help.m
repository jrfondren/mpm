:- module mpm.help.

:- interface.
:- import_module io.

:- pred print_usage(io::di, io::uo) is det.
:- pred print_version(io::di, io::uo) is det.


:- implementation.
:- import_module list.
:- import_module string.
:- import_module mpm.config.

print_version(!IO) :-
  io.format("%s\n", [s(mpm.config.version)], !IO).


print_usage(!IO) :-
  io.format("Usage: mpm <command> [<args>]\n", [], !IO),
  io.nl(!IO),
  io.format("Helper CLI to manage and package Mercury projects\n", [], !IO),
  io.nl(!IO),
  io.format("Commands:\n", [], !IO),
  io.format("   mpm make      -- Builds the current project\n", [], !IO),
  io.format("   mpm build     -- Builds the current project\n", [], !IO),
  io.format("   mpm init      -- Initilizes a new project, call this from the new project's directory\n", [], !IO),
  io.format("   mpm clean     -- Removes generated files for this project only, not the dependencies\n", [], !IO),
  io.format("   mpm realclean -- Removes generated fiels for this project, as well as it's dependencies\n", [], !IO),
  io.format("   mpm version   -- Prints the mpm version\n", [], !IO),
  io.nl(!IO).
