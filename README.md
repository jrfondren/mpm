mpm -- Mercury Project Manager
========================================================================================================================
This is an attempt at a mercury project manager, inspired by elm for the elm language.   It follows conventions users
of elm, npm, rebar, and erlang.mk will find familiar.


Current Status
------------------------------------------------------------------------------------------------------------------------
mpm is useable enough to build itself, but is not nearly as user friendly as I would like it to be.  Addtionally, it
is still very new, so please expect, and report, bugs.


Simple Tutorial
------------------------------------------------------------------------------------------------------------------------

```
mkdir my_project
cd my_project
mkdir src
mpm init
```

At this point edit the created mpm.json file.   In particular, add any dependencies you need.

Edit your mercury files, make sure they are in the ./src subdirectory.

When your ready to build, just type

```
mpm make
```

Dependency entries in mpm.json
------------------------------------------------------------------------------------------------------------------------
Ultimately, we would like to make an mpm command that does this for you.   For now, we must add these manually.

The dependencies section has the following form:

```
"dependencies": {
    "direct": {
        "mercury-json": {"source": "https://github.com/juliensf/mercury-json.git",
                         "type": "git",
                         "version": "mercury_14_01"}
    }
}

```

For most use cases, "direct" dependencies should suffice.   The "indirect" section is used for dependencies of
dependencies.   You can manually add entries here, but this section will be completely managed by mpm in the near
future.



Limitations
------------------------------------------------------------------------------------------------------------------------
Currently, we do not support windows.   MPM has a lot of file system interaction, and it will PROBABLY work in cygwin
or msys without issue provided that the "mmc" on the path is the Mercury compiler, and not the Microsoft Management
Console.

Git MUST be installed and on the path, we currently exec out to it directly.

Required system libraries cannot be specified, and users will get a link error.  For users used to JavaScript or
other high level languages, the link error will likely be indeciferable, so project maintainers are advised to
document these dependencies.   We have planned functionality to help address this with mpm.

Indirect dependencies (dependencies of dependencies) are not currently autodetected, though they should work fine if
manually specified in the mpm.json file.

Currently we compile everything in the default grade for the system.   Likely this is high-level C.  Eventually
I would like to add support for arbitrary grades (especially C#, Erlang, and Java) but I currently have no direct
need for this.

Static Linkage.   We link everything statically so it "just works" for most simple use cases.   This is not an ideal
solution in many cases, and we would like to handle those as well eventually, but getting "mpm install" working will
have to come first, since we'll have to ensure relevant shared libs are installed on the system when
we install the mpm managed application.


How to build
------------------------------------------------------------------------------------------------------------------------

1.  Have a version 14.01.1 of mercury installed, fetch it here:
    https://mercurylang.org/download.html
    * Newer vesions of the compiler may work, though they have stricter error
      detection and mpm may not build.   We are only targeting versioned
      mercury releases at this time, and not the ROTDs.
2.  Have make installed
3.  Type "make" in the repository root.
    *  If you don't have make, then cd into the ./src directory and issue:  mmc --make mpm


Installation
------------------------------------------------------------------------------------------------------------------------
Installation is currently manual, just copy the resultant executable (mpm) into your path somewhere.



Overview
------------------------------------------------------------------------------------------------------------------------
mpm is used to manage mercury projects for you.   It is not a replacement for mmc, rather is makes use of
mmc --make for actually building projects.

Where mpm adds value for you is for managing project dependencies.  Simply specify project dependencies in the
mpm.json file at the project's root, and mpm make will handle fetching them, building them, and linking against them.

Provided a project uses either mpm, custom make, or gnu autotools, and places it's constructed files (static libs, and
mercury init files) in a src subdirectory, mpm can manage it as a dependency for you.


Other Currently supported commands are:

*  mpm make -- Builds the current project
*  mpm build -- Builds the current project
*  mpm help -- Prints usage information
*  mpm version -- Prints the mpm version
*  mpm clean -- Removes generated files for this projects source, dependency
   packages are unmodified.
*  mpm realclean -- Removes generated files for this project, and all dependency
   packages.  Use this if you need to ensure dependencies are updated.

How it works under the hood
------------------------------------------------------------------------------------------------------------------------
Under the hood, "mpm make" just calls out to git and mmc for you.  That is, you could just
`cd ./src && mmc --make my_project` for simple applications with no dependencies.   As soon as you need dependencies
that are not installed on in the system, you have to start adding flags to mmc, mpm does this for you.

It is absolutely not intended to replace the functionality in mmc.   Rather, the goal is to let us easily download
3rd party libraries as packages and use them in our code.   That's not implemented yet.

Resul